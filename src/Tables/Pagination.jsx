import React from 'react'
import {inject, observer} from 'mobx-react'
import {getTranslation, uuid4} from "utils";
import "./TableComponent.scss"

@inject("tableStore")
@observer
export default class PaginationComponent extends React.Component {

    renderPages() {
        let page = this.props.tableStore.actions.page;
        let pages = this.props.tableStore.actions.pages;

        if (!pages || pages === 1) {
            return ''
        }

        let pagination = [];

        if (page > 2) {
            pagination.push({val: 1, active: false})
        }
        if (page >= 4) {
            pagination.push({val: "..."})
        }

        if (page === 1) {
            pagination.push({val: page, active: true})
        }

        if (page > 1) {
            pagination.push({val: page - 1, active: false});
            pagination.push({val: page, active: true})
        }

        if (page <= pages - 2) {
            pagination.push({val: page + 1, active: false});
        }

        if (page <= pages - 3) {
            pagination.push({val: "..."});
        }

        if (page <= pages - 1) {
            pagination.push({val: pages, active: false});
        }

        return (
            pagination.map(this.renderNavElement.bind(this))
        )

    };

    renderNavElement({active, val}) {
        let active_class = active ? "btn-danger" : "btn-secondary";
        return (
            <li className="m-nav__item m-nav__item--active p-0" key={uuid4()}>
                <button
                    onClick={active === false ? () => {
                        this.props.tableStore.actions.stopRefresh()
                        this.props.tableStore.actions.fetchByPage(val)
                        if (this.props.tableStore.paginationCallback) {
                            this.props.tableStore.paginationCallback(val)
                        }
                        this.props.tableStore.actions.startRefresh()
                    } : undefined}
                    className={`btn ${active_class} ${active === undefined ? "disabled" : ""} mr-1 m-btn m-btn--custom btn-sm  m-btn--bolder`}
                    style={{"minWidth": "38px"}}>
                    {val}
                </button>
            </li>
        )
    }

    renderCount = observer(() => {
        let count = this.props.tableStore.actions.count;
        return (
            <React.Fragment>
                {this.props.tableStore.actions.count > 0 &&
                <div className="table-count">
                    <div className="grey">
                        {this.props.tableStore.conf.labelCount || getTranslation("Count")}: {count}
                    </div>
                </div>}
            </React.Fragment>
        )
    });

    render() {
        return (
            <div className={this.props.className} id="pagination">
                {!this.props.tableStore.disableCount && <this.renderCount/>}
                <div className="pb-3 pt-3 pagination-wrap" key={uuid4()}>
                    <div className="m-stack__item m-topbar__nav-wrapper m--block-center">
                        <ul className="m-topbar__nav m-nav m-nav--inline">
                            {this.renderPages()}
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}