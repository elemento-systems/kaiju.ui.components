import React from "react";
import TableComponent, {TableDropdowns} from "./TableComponent";
import SortableScrollTable from "./SortableScrollTable";

export default TableComponent
export {
    SortableScrollTable,
    TableDropdowns
}