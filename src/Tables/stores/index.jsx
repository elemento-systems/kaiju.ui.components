import rowStore from "./rowStore";
import {TableStore} from "./tableStore";
import {SortableScrollTableStore} from "./sortableScrollTableStore";

export default TableStore;

export {
    rowStore, TableStore, SortableScrollTableStore
}
