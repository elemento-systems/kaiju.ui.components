import {action, computed, observable, toJS} from "mobx";

export default class RowStore {
    @observable row = observable.map({});

    constructor(data, store) {
        this.data = data;
        this.row.merge(data);
        this.store = store;
        this.initCheck()
    }

    initCheck() {
        if (this.checked) {
            if (this.store.checkMulti === false) {
                this.store.checkedRows.clear();
            }
            this.store.checkedRows.set(this.id, true);
        }
    }

    @computed get checked() {
        if (this.store.actions.allCheckedMode) {
            return !this.store.unCheckedRows.has(this.id)
        }

        return this.store.checkedRows.has(this.id)
    }

    @computed get id() {
        if (this.store.idKey) {
            return (toJS(this.row)[this.store.idKey] || {}).value
        }

        return Object.values(toJS(this.row))[0].id
    }

    get isSystem() {
        return Object.values(this.data)[0]["is_system"] === true;
    }

    get isDefault() {
        return Object.values(this.data)[0]["is_default"] === true;
    }

    @action toggleChecked() {
        if (this.checked) {
            this.setNotChecked()
        } else {
            this.setChecked()
        }
        this.store.checkCallback ? this.store.checkCallback(this) : null
    }

    @action
    setNotChecked() {
        if (this.store.actions.allCheckedMode) {
            this.store.unCheckedRows.set(this.id, true)
        } else {
            this.store.checkedRows.delete(this.id);
        }
    }

    @action
    setChecked() {
        if (this.store.checkMulti === false) {
            this.store.checkedRows.clear();
            this.store.checkedRows.set(this.id, true);
            return
        }

        if (this.store.actions.allCheckedMode) {
            this.store.unCheckedRows.delete(this.id)
        } else {
            this.store.checkedRows.set(this.id, true);
        }

    }
}
