export default class BaseStorage {
    static setItem(key, value) {
        window.sessionStorage.setItem(key, JSON.stringify(value))
    }

    static getItem(key, def) {
        let value = window.sessionStorage.getItem(key);
        try {
            value = JSON.parse(value)
        } catch (e) {
            value = def
        }

        return value !== undefined ? value : def
    }

    static removeItem(key) {
        window.sessionStorage.removeItem(key)
    }
}
