import React from "react";
import Breadcrumbs from "./Breadcrumbs";
import Searchbox, {SearchComponent} from "SearchComponent";
import {ButtonDropdown, ButtonSuccess, DoubleButtonDropdown} from "Buttons";

class DefaultHeader extends React.Component {
    render() {
        return (
            <div className={`col-12 flex pb-4 pl-0 pr-0 ${this.props.className || ""}`}>
                <div className="mr-auto" style={{fontSize: "14px", minHeight: "60px"}}>
                    {this.props.breadcrumbs && <Breadcrumbs crumbs={this.props.breadcrumbs}/>}

                    <div className="pt-4 mt-2" style={{minHeight: "43px"}}>
                        {this.props.searchConf &&
                        <SearchComponent
                            {...this.props.searchConf}/>
                        }

                        {this.props.searchCallback &&
                        <Searchbox
                            disabled={this.props.searchDisabled}
                            callback={this.props.searchCallback}/>}

                        {this.props.label &&
                        <span className="m--icon-font-size-lg3 m--font-bolder">{this.props.label}</span>
                        }

                        {this.props.children &&
                        this.props.children}
                    </div>

                </div>

                {this.props.settings &&
                <div className="m-dropdown m-dropdown--inline  m-dropdown--align-right pr-4 pt-2">
                    <i className="icon-gear fs-22 pointer" style={{fontSize: "30px"}} onClick={this.props.settings}/>
                </div>
                }

                {
                    this.props.buttonConf &&
                    <div className="m-dropdown m-dropdown--inline  m-dropdown--align-right">
                        <ButtonSuccess {...this.props.buttonConf}/>

                        {this.props.buttonConf.labelInfo &&
                        <div style={{
                            position: "absolute",
                            right: "0",
                            width: "400px",
                            textAlign: "right",
                        }}>
                            <i className="icon-warn pr-3" style={{color: "orange", position: "relative", top: "2px"}}/>
                            {this.props.buttonConf.labelInfo}
                        </div>
                        }
                    </div>
                }

                {
                    this.props.doubleButtonConf &&
                    <div className="m-dropdown m-dropdown--inline  m-dropdown--align-right">
                        <DoubleButtonDropdown conf={this.props.doubleButtonConf}/>

                        {this.props.doubleButtonConf.labelInfo &&
                        <div style={{
                            position: "absolute",
                            right: "0",
                            width: "400px",
                            textAlign: "right",
                        }}>
                            <i className="icon-warn pr-3" style={{color: "orange", position: "relative", top: "2px"}}/>
                            {this.props.doubleButtonConf.labelInfo}
                        </div>
                        }
                    </div>
                }

                {
                    this.props.dropdownConf &&
                    <div className="m-dropdown m-dropdown--inline  m-dropdown--align-right">
                        <ButtonDropdown conf={this.props.dropdownConf}/>
                    </div>
                }


            </div>
        )
    }
}

export default class Header extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        const className = `page-header d-flex align-items-center 
        ${this.props.className ? this.props.className : ''}
        pr-5 pl-5 ${this.props.noBorder ? '' : 'border-bottom'} `;

        return (
            <div className={className}>
                <DefaultHeader
                    {...this.props}
                    className="pt-4"
                />
            </div>
        )
    }
}

export {DefaultHeader};
