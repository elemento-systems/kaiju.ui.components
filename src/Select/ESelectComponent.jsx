import {observer} from "mobx-react";
import React from "react";
import AsyncPaginate from "react-select-async-paginate";
import ESelectStore from "./store";
import {LoadingMessage, NoOptions} from "./common";
import "./ESelectComponent.scss"

export default class ESelectComponent extends React.Component {
    constructor(props) {
        super(props);
        this.store = new ESelectStore({conf: this.props.conf, onChange: props.onChange})
    }
    reRender = observer(()=>{
        return (
            <AsyncPaginate
                value={this.store.getValue}
                components={{
                    LoadingMessage: LoadingMessage,
                    NoOptionsMessage: NoOptions
                }}
                loadOptions={this.store.loadOptions}
                defaultOptions
                debounceTimeout={this.store.debounceTimeout}
                // cacheUniq
                isDisabled={this.props.conf.disabled === true || this.store.isFetching}
                isClearable={this.props.conf.isClearable !== false}
                isMulti={this.props.conf.isMulti || false}
                // shouldLoadMore={this.store.shouldLoadMore}
                onChange={(vals, actions, e) => this.store.addValue(vals, actions, e)}
                closeMenuOnSelect={!(this.props.conf.isMulti || false)}
                placeholder={""}
                additional={{
                    page: 1,
                }}
            />
        )
    });

    render() {
        return <this.reRender />
    }
}
