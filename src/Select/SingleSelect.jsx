import {observer} from "mobx-react";
import React from "react";
import AsyncPaginate from "react-select-async-paginate";
import ESelectStore from "./store";
import {components} from 'react-select';
import {customStyles, DropDownIcon, LoadingMessage, NoOptions} from "./common";

@observer
export default class ESingleSelectComponent extends React.Component {
    constructor(props) {
        super(props);
        this.store = new ESelectStore({conf: this.props.conf, onChange: props.onChange})

        // убираем часть компонентов
        this.selectComponents = {
            SingleValue: this.makeSingleValueComponent(),
            DropdownIndicator: null,
            indicatorSeparator: null,
            LoadingIndicator: null,
            LoadingMessage: LoadingMessage,
            NoOptionsMessage: NoOptions
        }
    }

    makeSingleValueComponent() {
        // Новый компонент одиночного значения без бордеров и тд.

        return ({children, ...props}) => {
            return (
                <components.SingleValue {...props}>
                    {this.store.conf.label &&
                    <span style={{fontWeight: 600}}
                    >{this.store.conf.label}: </span>
                    }
                    {children}
                    <DropDownIcon {...this.props}/>
                </components.SingleValue>
            )
        };
    }

    render() {
        return (
            <AsyncPaginate
                value={this.store.getValue}
                loadOptions={this.store.loadOptions}
                defaultOptions
                debounceTimeout={this.store.debounceTimeout}
                // cacheUniq

                components={this.selectComponents}
                isClearable={false}
                isSearchable={this.props.conf.isSearchable}

                styles={customStyles}
                isDisabled={this.store.isFetching}

                isMulti={this.props.conf.isMulti || false}
                shouldLoadMore={this.store.shouldLoadMore}
                onChange={(vals, actions, e) => this.store.addValue(vals, actions, e)}
                closeMenuOnSelect={!(this.props.conf.isMulti || false)}
                placeholder={""}
                menuPlacement={this.props.conf.menuPlacement || "auto"}
                additional={{
                    page: 1,
                }}
            />
        )
    }
}

