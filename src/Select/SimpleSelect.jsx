import React from "react";
import Select, {components} from 'react-select';
import {customStyles, DropDownIcon, LoadingMessage, NoOptions} from "./common";


/* Simple select with options
conf.options = [
  { value: 'vanilla', label: 'Vanilla', rating: 'safe' },
  { value: 'chocolate', label: 'Chocolate', rating: 'good' },
  { value: 'strawberry', label: 'Strawberry', rating: 'wild' },
  { value: 'salted-caramel', label: 'Salted Caramel', rating: 'crazy' },
];

conf.defaultValue = [{ value: 'vanilla', label: 'Vanilla', rating: 'safe' }]

conf.onChange = (option) => console.log(option)
// { value: 'vanilla', label: 'Vanilla', rating: 'safe' }
 */
export class SimpleSelectComponent extends React.Component {
    constructor(props) {
        super(props);
        this.conf = this.props.conf;

        // убираем часть компонентов
        this.selectComponents = {
            SingleValue: this.makeSingleValueComponent(),
            DropdownIndicator: null,
            indicatorSeparator: null,
            LoadingIndicator: null,
            LoadingMessage: LoadingMessage,
            NoOptionsMessage: NoOptions
        }
    }

    makeSingleValueComponent() {
        // Новый компонент одиночного значения без бордеров и тд.

        return ({children, ...props}) => {
            return (
                <React.Fragment>
                    {
                        this.conf.onlyIcon &&
                        <components.SingleValue {...props}>
                            <DropDownIcon {...this.props}/>
                        </components.SingleValue>
                    }

                    {
                        !this.conf.onlyIcon &&
                        <components.SingleValue {...props}>
                            {
                                this.conf.label &&
                                <span style={{fontWeight: 600}}>{this.conf.label}:</span>
                            }
                            {children}
                            <DropDownIcon {...this.props}/>
                        </components.SingleValue>
                    }
                </React.Fragment>
            )
        };
    }

    render() {
        const viewConf = this.conf.selectView ? {} : {
            styles: customStyles,
            components: this.selectComponents
        };

        return (
            <Select
                defaultValue={this.conf.defaultValue}
                options={this.conf.options}

                {...viewConf}

                isClearable={false}
                isSearchable={false}
                // isDisabled={this.store.isFetching}
                // isMulti={this.props.conf.isMulti || false}
                onChange={(vals, actions, e) => this.conf.onChange(vals, actions, e)}
                // closeMenuOnSelect={!(this.props.conf.isMulti || false)}
                placeholder={""}

                menuPlacement={this.props.conf.menuPlacement || "auto"}

            />
        )
    }
}