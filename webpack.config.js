const path = require("path");
// const HtmlWebpackPlugin = require("html-webpack-plugin");
// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
// var OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
// const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: path.join(__dirname, "/src"),
    output: {
        path: path.join(__dirname, "/dist"),
        filename: "index_bundle.js"
    },
    optimization: {
        splitChunks: {
            chunks: 'initial',
            filename: "vendor.js",
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/
                }

            }
        }
    },
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                },
            },
            {
                test: /\.(scss|sass)$/i,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'},
                    {
                        loader: 'sass-loader',
                        options: {
                            sassOptions: {
                                includePaths: ['/src']
                            }
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    plugins: [],
    resolve: {
        extensions: ['.js', '.jsx']
    }
};